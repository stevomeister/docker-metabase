FROM eclipse-temurin:17

ARG MB_VERSION
RUN useradd -ms /bin/sh metabase
USER metabase
WORKDIR /home/metabase
RUN curl https://downloads.metabase.com/$MB_VERSION/metabase.jar --output ./metabase.jar
CMD ["java", "-jar", "metabase.jar"]
